# Node Task

This project contains node hello-world

```js
const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a,b) => a+b;

app.get('/add', (req, res) => {
    let a = 4;
    let b = 5;
    res.send(`Adding ${a} and ${b} = ${add(a,b)}`)
})

app.get('/add/:a/:b', (req, res) => {
    let a = req.params.a;
    let b = req.params.b;
    if(isNaN(a) || isNaN(b)){
        res.send(`Both params arent numbers: a=${a} b=${b}`);
        return;
    }

    res.send(`Adding ${a} and ${b} = ${add(parseFloat(a),parseFloat(b))}`)
})

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
```

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal commands

Code block:

```sh
git clone https://gitlab.com/Aleksi18004/node-task
cd ./node-task
npm i
```

### Start cmd: `npm start`

## App should work
![alt text for test image](https://d1ddzfo1d7bgrb.cloudfront.net/aamulehti/200499461-800x_.jpg)|
